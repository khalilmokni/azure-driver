import os

_azure_client_resource_group = os.getenv('AZURE_RESOURCE_GROUP_NAME')
_azure_client_id = os.getenv('AZURE_CLIENT_ID')
_azure_client_secret = os.getenv('AZURE_CLIENT_SECRET')
_azure_tenant_id = os.getenv('AZURE_TENANT_ID')
_azure_subscription_id = os.getenv('AZURE_SUBSCRIPTION_ID')
_azure_vnet = os.getenv('AZURE_VIRTUAL_NETWORK_NAME')
_azure_subnet= os.getenv('AZURE_SUBNET_NAME')
_azure_security_grp = os.getenv('AZURE_SECURITY_GROUP_NAME')