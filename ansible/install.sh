#!/usr/bin/env bash

environment_set() {
  env="${1}"
  env_file="env/${env}.yml"
  export ENV_FILE="${env_file}"
  export PLAYBOOK="playbook-${env}.yml"
  export VAROPTS="@${env_file}"
  cp "${env_file}" "${env_file}.bak"
  public_ip="$(curl https://ip.comwork.io 2>/dev/null)"
  sed -i "s/VAR_PUBLIC_IP/${public_ip}/g" "${env_file}"
}

install_ansible() {
  if lsb_release -d >/dev/null 2>&1; then
    install_cmd="apt update -y && apt install -y git ansible"
  else
    install_cmd="yum install -y epel-release && yum install -y ansible git"
  fi

  if ! ansible-playbook --version 2>/dev/null; then
    eval "${install_cmd}"
  fi
}

while getopts ":e:" option; do 
  case "$option" in 
    e) environment_set $OPTARG ;;
    esac
done

echo "${VAROPTS}"
install_ansible
sudo ansible-playbook $PLAYBOOK -e "$VAROPTS"
mv "${ENV_FILE}.bak" "${ENV_FILE}"
