# Ansible gitops configuration

Welcome to comwork cloud. Here you'll find the ansible playbooks, roles and configurations of your services.

![cloud_bg](https://gitlab.comwork.io/comwork_public/comwork_cloud/-/raw/main/img/cloud_bg.png)

Each push on this repo will trigger a pipeline using gitlab-ci that will run the impacted playbooks.

You'll find:
* The ansible roles [here](./roles)
* The playbook are the files named `playbook-{your instance}.yml`
* The environment files (`env/{your service}.yaml`) and documentation (`env/{your service}.md`) are [here](./env).

Sometimes, you'll have to follow the instructions of the `env/{your service}.md` to make it work after provisionning to make it work.

You also can check our public documentation [here](https://gitlab.comwork.io/comwork_public/comwork_cloud)
