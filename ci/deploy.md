# Force redeploy

This markdown has only this purpose: allow to retrigger CI/CD without changing sources.

## History

* 29/06/2022: first delivery
* 18/09/2022: new delivery
* 01/12/2022: new delivery (scaleway pricing increase)
* 02/08/2023: new delivery
* 01/09/2023: new delivery (fastapi)
* 02/09/2023: new delivery (fastapi migration)
* 14/11/2023: new delivery (import environment)
* 08/12/2023: new delivery (import faas)
* 19/12/2023: new delivery (test runner)
