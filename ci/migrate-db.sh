#!/usr/bin/env bash

source ./ci/compute-env.sh "whatever"

echo "Pulling flyway image"
docker pull "${FLYWAY_IMAGE}:${FLYWAY_IMAGE_TAG}"

echo "Running database migration for ${ENV}"
docker run -v ${PWD}/src/migrations:/flyway/sql \
  --rm "${FLYWAY_IMAGE}:${FLYWAY_IMAGE_TAG}" \
  -mixed=true \
  -url="jdbc:postgresql://${CI_POSTGRES_HOST}:${CI_POSTGRES_PORT}/${DB_NAME}" \
  -user="${CI_POSTGRES_USER}" \
  -password="${CI_POSTGRES_PASSWORD}" migrate
