#!/usr/bin/env bash

source ./ci/compute-env.sh "whatever"

dir_to_override=(
    "img"
    "src"
)

for dir in "${dir_to_override[@]}"; do
    rm -rf "${DIR_MIRROR}/${dir}"
    cp -R "${dir}" "${DIR_MIRROR}"
done

files_to_remove=(
    "src/adapters/payments/PaymentAdapter.py"
)

for file in "${files_to_remove[@]}"; do
    rm -rf "${DIR_MIRROR}/${file}"
done

cp .mgitlab-ci.yml "${DIR_MIRROR}/.gitlab-ci.yml"

mkdir -p "${DIR_MIRROR}/ci"
cp ci/pmirror.sh "${DIR_MIRROR}/ci/mirror.sh"

files_to_copy=(
    "docker-compose-local.yml"
    "docker-compose-build.yml"
    "manifest.json"
    "LICENCE"
    "Dockerfile"
    "CONTRIBUTING.md"
    "README.md"
    ".env.dist"
    "Pulumi.yaml"
    "cloud-init.sh.j2"
    "cloud-init.yml.j2"
    "cloud_environments_local.yml.dist"
    "ansible_script.sh"
    ".gitignore"
    ".dockerignore"
    "requirements.txt"
    "VERSION"
)

for file in "${files_to_copy[@]}"; do
    cp "${file}" "${DIR_MIRROR}"
done

cd "${DIR_MIRROR}"
git add .
git commit -m "Release: ${VERSION_SHA}"
git push origin main
