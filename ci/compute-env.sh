#!/usr/bin/env bash

export CWCLOUD_APP="${1}"

if [[ ! ${CWCLOUD_APP} ]]; then
  echo "Missing CWCLOUD_APP parameter" >&2
  exit 1
fi

export FLYWAY_IMAGE="flyway/flyway"
export FLYWAY_IMAGE_TAG="10"
export DIR_MIRROR="/home/centos/cwcloud-api"

export VERSION="${CI_COMMIT_BRANCH}-${CI_COMMIT_SHORT_SHA}"
export VERSION_SHA="${VERSION}"
if [[ $CI_COMMIT_BRANCH == "main" ]]; then
  export VERSION="ce-$(grep -oE "^[0-9\.]+$" VERSION)"
  export VERSION_SHA="${VERSION}-${CI_COMMIT_SHORT_SHA}"
fi

export ENV="ppd"
export CI_REGISTRY="rg.fr-par.scw.cloud/cwcloud-ce-u7u1q0"
export DB_NAME="${ENV}cloudapidb"

export SLACK_TRIGGER="on"
export GITLAB_URL="https://gitlab.comwork.io"
export EMAIL_EXPEDITOR="cloud@comwork.io"
export EMAIL_ACCOUNTING="accounting@comwork.io"
export LOG_LEVEL="INFO"
export LOG_FORMAT="json"
export INVOICE_BUCKET_NAME=${PPD_INVOICE_BUCKET_NAME}
export DOMAIN="https://ppd.cloud.comwork.io"
export DEFAULT_PROVIDER="scaleway"
export COMPANY_NAME="Comwork PPD"
export STRIPE_API_KEY="${STRIPE_API_KEY_PPD}"
export STRIPE_WEBHOOK_SECRET="${STRIPE_WEBHOOK_SECRET_PPD}"
export DISCORD_TOKEN="${DISCORD_TOKEN_PPD}"
export DISCORD_TOKEN_PUBLIC="${DISCORD_TOKEN_PUBLIC_PPD}"
export DYNAMIC_REPO_GROUPID="${CI_PPD_DYNAMIC_REPO_GROUPID}"
## The pricing is auto-generated in this repo: https://gitlab.comwork.io/comwork_public/comwork_cloud.git
curl "https://gitlab.comwork.io/comwork_public/comwork_cloud/-/raw/main/prices_env.sh" -o prices_env.sh 2>/dev/null
chmod +x prices_env.sh
source prices_env.sh

export INVOICE_COMPANY_NAME="COMWORKIO"
export INVOICE_COMPANY_SIGNATURE="{{ invoice_company_signature }}"
export INVOICE_COMPANY_REGISTRATION_NUMBER="838 757 987 00014"
export INVOICE_COMPANY_ADDRESS="128 rue de la Boétie, 75008 Paris"
export INVOICE_COMPANY_CONTACT="M. Idriss Neumann"
export INVOICE_COMPANY_EMAIL="idriss.neumann@comwork.io"
export INVOICE_COMPANY_LOGO="https://cloud-assets.comwork.io/assets/logos/comwork-logo.png"
export INVOICE_COMPANY_SIGNATURE="https://cloud-assets.comwork.io/assets/cachets/comworkio.png"
export INVOICE_COMPANY_CITY="Paris"
export BILLING_PROCEDURE_URL="https://doc.cloud.comwork.io/docs/tutorials/console/public/billing"
export CWAI_API_URL="https://cwai-api.comwork.io"

export API_PRICE_EMAIL=5
export API_PRICE_CWAI=1

export CONSUMER_SLEEP_TIME=3600
export FAAS_WAIT_STARTUP_TIME=30
export FAAS_API_URL="https://ppd.cloud-api.comwork.io"
export CONSUMER_CHANNEL="faasppd"
export TRIGGERS_CHANNEL="faastriggersppd"
export FAAS_API_TOKEN="${FAAS_API_TOKEN_PPD}"

if [[ $CI_COMMIT_BRANCH == "main" ]]; then
  export ENV="prod"
  export DB_NAME="cloudapidb"
  export LOG_LEVEL="INFO"
  export INVOICE_BUCKET_NAME=${PROD_INVOICE_BUCKET_NAME}
  export DOMAIN="https://cloud.comwork.io"
  export COMPANY_NAME="Comwork"
  export STRIPE_API_KEY="${STRIPE_API_KEY_PROD}"
  export STRIPE_WEBHOOK_SECRET="${STRIPE_WEBHOOK_SECRET_PROD}"
  export DISCORD_TOKEN="${DISCORD_TOKEN_PROD}"
  export DISCORD_TOKEN_PUBLIC="${DISCORD_TOKEN_PUBLIC_PROD}"
  export DYNAMIC_REPO_GROUPID="${CI_PROD_DYNAMIC_REPO_GROUPID}"
  export FAAS_API_URL="https://cloud-api.comwork.io"
  export CONSUMER_CHANNEL="faas"
  export TRIGGERS_CHANNEL="faastriggers"
  export FAAS_API_TOKEN="${FAAS_API_TOKEN_PROD}"
fi

if [[ $ENV == "ppd" ]]; then
  export PRICE_t2_small="0.0391"
  export PRICE_t2_medium="0.0789"
  export PRICE_t2_large="0.1893"
fi

export GCP_NETWORK=default

export CONSUMER_GROUP="${CONSUMER_CHANNEL}"
export TRIGGERS_GROUP="${TRIGGERS_CHANNEL}"

export IMAGE_NAME="cwcloud-${CWCLOUD_APP}"
export SERVICE_NAME="comwork_cloud_${CWCLOUD_APP}"
export DEPLOYED_SERVICE_NAME="${SERVICE_NAME}_${ENV}"
